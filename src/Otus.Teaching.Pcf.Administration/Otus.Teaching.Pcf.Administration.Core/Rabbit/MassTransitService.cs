﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace Otus.Teaching.Pcf.Administration.Core.Rabbit
{
	public class MassTransitService : IHostedService
	{
		private readonly IBusControl _busControl;

		public MassTransitService(IBusControl busControl)
		{
			_busControl = busControl;
		}

		public async Task StartAsync(CancellationToken cancellationToken)
		{
			Console.WriteLine($"Start MassTransitService");
			await _busControl.StartAsync(cancellationToken);
		}

		public async Task StopAsync(CancellationToken cancellationToken)
		{
			await _busControl.StopAsync(cancellationToken);
			Console.WriteLine($"Stop MassTransitService");
		}
	}
}