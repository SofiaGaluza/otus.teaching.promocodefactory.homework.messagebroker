﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;

namespace Otus.Teaching.Pcf.Administration.Core.Rabbit
{
	public class PromoCodeConsumer : IConsumer<PromoCodeRabbitModel>
	{
		private readonly IEmployeeService _employeeService;

		public PromoCodeConsumer(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		public async Task Consume(ConsumeContext<PromoCodeRabbitModel> context)
		{
			if (context.Message.PartnerManagerId == null)
				throw new Exception($"{nameof(Consume)}: employee id is null");
				
			var id = (Guid)context.Message.PartnerManagerId;
			var employee = await _employeeService.GetByIdAsync(id);

			if (employee == null)
				throw new Exception($"{nameof(Consume)}: employee with id = {id} is not found ");
			
			employee.AppliedPromocodesCount++;

			await _employeeService.UpdateAsync(employee);
		}
	}
}