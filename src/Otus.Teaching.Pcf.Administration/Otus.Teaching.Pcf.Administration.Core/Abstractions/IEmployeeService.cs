﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions
{
	public interface IEmployeeService
	{
		Task<IEnumerable<Employee>> GetAllAsync();
		Task<Employee> GetByIdAsync(Guid id);
		Task UpdateAsync(Employee entity);
	}
}