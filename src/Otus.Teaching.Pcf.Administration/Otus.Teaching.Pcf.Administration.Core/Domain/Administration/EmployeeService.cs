﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
	public class EmployeeService : IEmployeeService
	{
		private readonly IRepository<Employee> _employeeRepository;

		public EmployeeService(IRepository<Employee> employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		public async Task<IEnumerable<Employee>> GetAllAsync()
		{
			return await _employeeRepository.GetAllAsync();
		}

		public async Task<Employee> GetByIdAsync(Guid id)
		{
			return await _employeeRepository.GetByIdAsync(id);
		}

		public async Task UpdateAsync(Employee entity)
		{
			await _employeeRepository.UpdateAsync(entity);
		}
	}
}