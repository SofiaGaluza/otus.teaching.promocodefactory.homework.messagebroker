﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers
{
    public class RabbitMapper
    {
        public static PromoCode MapFromDto(PromoCodeRabbitModel model)
        {
	        return new PromoCode()
	        {
		        Id = model.Id,
		        Code = model.Code,
		        ServiceInfo = model.ServiceInfo,
		        BeginDate = model.BeginDate,
		        EndDate = model.EndDate,
		        PartnerId = model.PartnerId,
		        PartnerManagerId = model.PartnerManagerId,
		        PreferenceId = model.PreferenceId
	        };
		}

        public static PromoCodeRabbitModel MapToDto(PromoCode promoCode)
        {
	        return new PromoCodeRabbitModel()
	        {
		        Id = promoCode.Id,
		        Code = promoCode.Code,
		        ServiceInfo = promoCode.ServiceInfo,
		        BeginDate = promoCode.BeginDate,
		        EndDate = promoCode.EndDate,
		        PartnerId = promoCode.PartnerId,
		        PartnerManagerId = promoCode.PartnerManagerId,
		        PreferenceId = promoCode.PreferenceId,
	        };
		}
    }
}
