﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Rabbit
{
	public class ProducerService : IProducerService
	{
		private readonly IBusControl _busControl;
		
		public ProducerService(IBusControl busControl)
		{
			_busControl = busControl;
		}

		public async Task PublishGivingPromoCodeToCustomer(PromoCode promoCode)
		{
			await _busControl.Publish(RabbitMapper.MapToDto(promoCode));
		}
	}
}