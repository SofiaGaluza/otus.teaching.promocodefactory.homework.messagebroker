﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions
{
	public interface IProducerService
	{
		public Task PublishGivingPromoCodeToCustomer(PromoCode promoCode);
	}
}